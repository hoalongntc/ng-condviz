function ngCondviz() {
  var directive = {};
  directive.templateUrl = 'ng-condviz/ng-condviz';
  directive.scope = {
    ngCondviz: '=',
    ngCondvizOptions: '=?'
  };
  directive.controller = ['$scope', '$timeout', function(scope, $timeout) {
    var options = angular.extend({
        templateUrl: 'ng-condviz/ng-condviz-node',
        emptyTemplateUrl: 'ng-condviz/ng-condviz-empty'
    }, scope.ngCondvizOptions);
    var self = this;

    var findParent = function(path, current, node) {
      if (!current) return null;
      var result;
      for(var i = 0; i < current.length && !result; i++) {
        if (current[i] == node) {
          result = {path: path, current: current, index: i};
        }
        if (!result && current[i].and) {
          result = findParent('and', current[i].and, node);
        }
        if (!result && current[i].or) {
          result = findParent('or', current[i].or, node);
        }
      }
      return result;
    };
    var _shorten = function(node) {
      if (!node) return;
      var path = node.and ? 'and' : (node.or ? 'or': null);

      if (!path) {
        return node;
      } else if (node[path].length == 0) {
        return null;
      } else if (node[path].length == 1) {
        return _shorten(node[path][0]);
      } else {
        return node;
      }
    };
    var _flatten = function(level, current) {
      if (!current) return {node: 0, level: level};
      var node = 0;
      var maxLevel = level;
      for(var i = 0; i < current.length; i++) {
        var shortenResult = _shorten(current[i]);
        if (shortenResult) {
          current[i] = shortenResult;

          var path = current[i].and ? 'and' : (current[i].or ? 'or': null);
          if (path) {
            var result = _flatten(level + 1, current[i][path]);
            node += result.node;
            if (result.level > maxLevel) {
              maxLevel = result.level;
            }
          } else {
            node ++;
          }
          current[i].level = level;
        } else {
          current.splice(i--, 1);
        }
      }
      return {node: node, level: maxLevel};
    };
    var flatten = function() {
      var result = _flatten(0, scope.ngCondviz);
      scope.totalNode = result.node;
      scope.maxLevel = result.level;
    };

    self.addNode = function(node, type) {
      const nodeInfo = findParent(null, scope.ngCondviz, node);
      if (nodeInfo) {
        $timeout(function() {
          var newNode;
          if (type == nodeInfo.path) {
            newNode = {data: angular.copy(options.defaultData)};
            nodeInfo.current.splice(nodeInfo.index + 1, 0, newNode);
          } else {
            newNode = {};
            newNode[type] = [];
            newNode[type].push({data: node.data});
            newNode[type].push({data: angular.copy(options.defaultData)});
            nodeInfo.current.splice(nodeInfo.index, 1, newNode);
          }
          flatten();
        });
      }
    };
    self.removeNode = function(node) {
      const nodeInfo = findParent(null, scope.ngCondviz, node);
      if (nodeInfo) {
        $timeout(function() {
          nodeInfo.current.splice(nodeInfo.index, 1);
          flatten();
        });
      }
    };
    self.totalNode = function () {
      return scope.totalNode;
    };
    self.maxLevel = function () {
      return scope.maxLevel;
    };
    self.getTemplateUrl = function() {
      return options.templateUrl;
    };
    self.condvizOptions = options;

    scope.totalNode = 0;
    scope.maxLevel = 0;
    scope.addFirstNode = function() {
      scope.ngCondviz = [];
      scope.ngCondviz.push({data: angular.copy(options.defaultData), level: 0});
      flatten();
    };
    scope.getEmptyTemplateUrl = function () {
      return options.emptyTemplateUrl;
    };

    scope.$watch('ngCondviz', flatten);
    $timeout(flatten);
  }];
  directive.controllerAs = 'ngCondviz';

  return directive;
}

var m = angular.module('ngCondviz', []);
m.run(['$templateCache', require('./ng-condviz-template')]);
m.directive('ngCondvizBlock', require('./ng-condviz-block'));
m.directive('ngCondvizNode', require('./ng-condviz-node'));
m.directive('ngCondviz', ngCondviz);

module.exports = m.name;
