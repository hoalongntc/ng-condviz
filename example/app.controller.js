import angular from 'angular';

class AppCtrl {
  constructor() {
    this.body ='Hello from controller';
  }
}

export default angular
  .module('app.controller.AppCtrl', [])
  .controller('AppCtrl', AppCtrl);
