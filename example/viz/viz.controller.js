import './viz.tpl.jade';
import './viz.less';

class VizCtrl {
  constructor() {
    this.body = 'Hello from viz controller';
    this.vizData = [];
    this.vizOptions = {
      templateUrl: 'vizNodeTest',
      emptyTemplateUrl: 'vizNodeEmpty',
      defaultData: {field: null, type: 'no_data', value: null}
    };
  }
}

function template($templateCache) {
  var nodeTemplate = '' +
    '<div ng-style="{paddingLeft: ((maxLevel() - node.level) * 15) + \'px\'}">Level: {{node.level}}/{{maxLevel()}}: ' +
    '<input type="text" style="width: 100px;" ng-model="node.data.field"/> ' +
    '<button ng-click="addAndNode()">And</button>' +
    '<button ng-click="addOrNode()">Or</button>' +
    '<button ng-click="removeNode()" ng-hide="totalNode() < 2">Delete</button>' +
    '</div>';

  var emptyTemplate = '<div>Empty <button ng-click="addFirstNode()">Add One</button></div>';

  $templateCache.put('vizNodeTest', nodeTemplate);
  $templateCache.put('vizNodeEmpty', emptyTemplate);
}

export default angular
  .module('app.controller.VizCtrl', [])
  .run(['$templateCache', template])
  .controller('VizCtrl', VizCtrl);
