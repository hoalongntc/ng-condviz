## Angular condition visualization

#### Install
```
npm install git+https://gitlab.com/hoalongntc/ng-condviz.git
```

#### Import
Support browserify, webpack, systemjs
```
import from 'ng-condvid'

angular
  .module('app', ['ngCondviz'])
  ...
```

Or
```
angular
  .module('app', [require('ng-condviz')])
  ...
```

#### Usage
```
<div ng-condviz="data" ng-condviz-options="options"></div>
```

`data`: Your condition tree object. Data structure:
```
[
  {data: 'Node 1'},
  {and: [
    {data: 'Node 2'},
    {or: [
      {data: 'Node 3'},
      {or: [
        {data: 'Node 4'},
        {data: 'Node 5'}
      ]},
      {data: 'Node 6'},
      {and: [
        {data: 'Node 7'},
        {data: 'Node 8'}
      ]}
    ]}
  ]},
  {or: [
    {data: 'Node 9'},
    {and: [
      {data: 'Node 10'},
      {data: 'Node 11'}
    ]},
    {data: 'Node 12'},
    {and: [
      {data: 'Node 13'},
      {data: 'Node 14'}
    ]}
  ]}
]
```

`options`: Configuration object

- `options.templateUrl`: Url to you custom node template. Default `ng-condviz/ng-condviz-node`
- `options.emptyTemplateUrl`: Url to you custom empty template. Default `ng-condviz/ng-condviz-empty`
- `options.defaultData`: Default data when creating new node

#### Node template
Node template is an normal angular template. Inside template, you can access node data and methods through:
- `node.level` to get current level
- `node.data` to get node data
- `addAndNode()` method to add an `And Node`
- `addOrNode()` method to add an `Or Node`
- `removeNode()` method to current remove

- `totalNode()` method to get total node count
- `maxLevel()` method to get height of tree
- `addFirstNode()` method to add first node. Use when tree is empty.

#### Example
See [`example`](https://gitlab.com/hoalongntc/ng-condviz/tree/master/example) folder for more detail
