import angular from 'angular';

angular.module('app', [
  require('../lib/ng-condviz'),

  require('./app.controller').name,
  require('./viz/viz.controller').name
]);
