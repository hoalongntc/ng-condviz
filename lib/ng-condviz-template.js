function template($templateCache) {
  var ngCondvizTemplate = '' +
    '<div ng-show="totalNode == 0"><ng-include src="getEmptyTemplateUrl()"></ng-include></div>' +
    '<div class="condviz-block condviz-block-data" ng-repeat-start="cond in ngCondviz" ng-if="cond.data" ng-condviz-node="cond"></div>' +
    '<div class="condviz-block condviz-block-and" ng-if="cond.and" ng-condviz-block="cond.and"></div>' +
    '<div class="condviz-block condviz-block-or" ng-repeat-end ng-if="cond.or" ng-condviz-block="cond.or"></div>';

  var ngCondvizBlockTemplate = '' +
    '<div class="condviz-block condviz-block-data" ng-repeat-start="cond in ngCondvizBlock" ng-if="cond.data" ng-condviz-node="cond"></div>' +
    '<div class="condviz-block condviz-block-and" ng-if="cond.and" ng-condviz-block="cond.and"></div>' +
    '<div class="condviz-block condviz-block-or" ng-repeat-end ng-if="cond.or" ng-condviz-block="cond.or"></div>';

  var ngCondvizNodeTemplate = '' +
    '<div class="condviz-leaf" data-condviz-level="{{node.level}}">' +
    '<button class="condviz-action condviz-and" data-condviz-action="and" ng-click="addAndNode()">&</button>' +
    '<button class="condviz-action condviz-or" data-condviz-action="or" ng-click="addOrNode()">||</button>' +
    '<button class="condviz-action condviz-remove" data-condviz-action="remove" ng-click="removeNode()" ng-hide="totalNode() < 2">x</button>' +
    '<span class="condviz-data" ng-bind="node.data">No data</span></div>';

  var ngCondvizEmptyTemplate = '' +
    '<div class="condviz-leaf">' +
    '<button class="condviz-action" data-condviz-action="and" ng-click="addFirstNode()">+</button>' +
    '<span class="condviz-data">No Node, add one</span>' +
    '</div>';

  $templateCache.put('ng-condviz/ng-condviz', ngCondvizTemplate);
  $templateCache.put('ng-condviz/ng-condviz-block', ngCondvizBlockTemplate);
  $templateCache.put('ng-condviz/ng-condviz-node', ngCondvizNodeTemplate);
  $templateCache.put('ng-condviz/ng-condviz-empty', ngCondvizEmptyTemplate);
}

module.exports = template;
