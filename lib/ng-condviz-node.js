function ngCondvizNode() {
  var directive = {};
  directive.require = '^ngCondviz';
  directive.template = '<ng-include src="getTemplateUrl()"></ng-include>';
  directive.restrict = 'A';
  directive.controller = ['$scope', function(scope) {
    scope.getTemplateUrl = function() {
      return scope.ctrl.getTemplateUrl();
    };
    scope.removeNode = function() {
      scope.ctrl.removeNode(scope.node);
    };
    scope.addAndNode = function() {
      scope.ctrl.addNode(scope.node, 'and');
    };
    scope.addOrNode = function() {
      scope.ctrl.addNode(scope.node, 'or');
    };
    scope.totalNode = function () {
      return scope.ctrl.totalNode();
    };
    scope.maxLevel = function () {
      return scope.ctrl.maxLevel();
    };
  }];
  directive.controllerAs = 'ngCondvizNode';
  directive.scope = {
    node: '=ngCondvizNode'
  };
  directive.link = function (scope, ele, attr, ngCondvizCtrl) {
    scope.ctrl = ngCondvizCtrl;
  };
  return directive;
}

module.exports = ngCondvizNode;
