function ngCondvizBlock() {
  var directive = {};
  directive.require = '^ngCondviz';
  directive.templateUrl = 'ng-condviz/ng-condviz-block';
  directive.scope = {
    ngCondvizBlock: '='
  };

  return directive;
}

module.exports = ngCondvizBlock;
